import pandas as pd
from pyplot.general import DotPlot

# Load players stats from 2018-19 NBA seasson
data = pd.read_csv("data/basketball.csv", sep=",", index_col=0)

# Filter data
data = data.dropna(axis=1, how='all')
data = data.dropna(axis=0, how='any')
data = data.loc[data['MP'] > 1000]

print(data.columns)
# Select the desired categories
titles = ['USG%', '3PAr', 'TRB%']

# Create a DotPlot
dot = DotPlot(data[titles])

# Plot data into DotPlot object
dot.plot("James Harden")
dot.plot("Giannis Antetokounmpo")

# Save data
dot.save("results/dotplot.png")
