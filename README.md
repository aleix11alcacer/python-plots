[![pipeline status](https://gitlab.com/aleix11alcacer/python-radar-chart/badges/master/pipeline.svg)](https://gitlab.com/aleix11alcacer/python-radar-chart/commits/master)
[![coverage report](https://gitlab.com/aleix11alcacer/python-radar-chart/badges/master/coverage.svg)](https://gitlab.com/aleix11alcacer/python-radar-chart/commits/master)
# Python plots

This package is a collection of plots that are implemented using *pandas*, *numpy* and *matplotlib*.

## Available plots

### General plots

- Radar Plot
- Dot Plot

### Specific plots

- Basketball shot chart plot (in progress...)


## Installation

```
python setup.py install
```

### Develop mode

```
python setup.py develop
```

```
python setup.py develop --uninstall
```

## Usage

All plots are constructed using a pandas DataFrame. The row indexes of the dataframe should contain an identificator of the row values. For example, if the DataFrame contains basketball stats, the row names can be the player or the team name. 

This row indexes are used to plot the values of a row. This action can be done multiple times. For example, two player statistics can be plotted on the same plot.

Finally, the plot can be stored in memory passing a filename.

### Examples

Complete examples of how to use this plots are located in the [examples](examples) folder. The following image is the result obtained when the [dot plot](examples/dot.py) example is executed:

![Basketball radar chart](results/dotplot.png)
