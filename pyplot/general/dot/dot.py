import matplotlib.pyplot as plt
import numpy as np
from math import fsum


class DotPlot:
    def __init__(self, df, reverse=None, title=None):
        self.df = df

        self.fig = plt.figure(figsize=(10, len(list(df)) * 3))

        self.reverse = [] if reverse is None else reverse
        for cat in self.reverse:
            if cat not in list(df):
                print(f"{cat} is not a DataFrame column")
                raise AttributeError

        self.categories = sorted(list(df))

        self.hist = list([None for _ in range(len(list(df)))])
        self.edges = list([None for _ in range(len(list(df)))])

        for ind, cat in enumerate(self.categories):
            self.fig.add_subplot(len(list(df)), 1, ind + 1)

            bins = 50
            hist, edges = np.histogram(self.df[cat].values, bins=bins)
            self.hist[ind] = hist
            self.edges[ind] = edges

            sum = [h for h in hist]
            for i in range(1, len(hist)):
                sum[i] += sum[i - 1]

            sum /= sum[len(hist) - 1]

            p95 = np.where(sum > 0.95)[0][0]
            p75 = np.where(sum > 0.75)[0][0]
            p25 = np.where(sum > 0.25)[0][0]

            y = np.arange(- (hist.max() + 1), hist.max() + 1, 2)
            x = np.arange(bins)
            X, Y = np.meshgrid(x, y)

            Y = Y.astype(np.float)
            Y[abs(Y) >= hist] = np.nan

            color = np.empty(X.shape, dtype='|S6')

            color[:] = 0
            color[X < p95] = 1
            color[X < p75] = 2
            color[X < p25] = 3

            plt.scatter(X, Y, s=150, c=color, cmap=plt.cm.summer)

            title = cat
            plt.title(title, fontsize=14)
            plt.axis('off')


    def plot(self, name):
        player_data = self.df.loc[name]

        for ind, cat in enumerate(self.categories):
            x = np.where(player_data[cat] <= self.edges[ind])[0][0] - 1
            plt.subplot(len(list(self.df)), 1, ind + 1).plot(x, 0,"o", c="r", markersize=15)
            plt.subplot(len(list(self.df)), 1, ind + 1).annotate(name, (x, 0), xytext=(x-1, self.hist[ind][x]+5), arrowprops={'arrowstyle': '->', 'shrinkB': -10})

    def save(self, filename):
        plt.savefig(filename)
