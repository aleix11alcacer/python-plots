import matplotlib.pyplot as plt
import numpy as np


class RadarPlot:
    def __init__(self, df, reverse=None, lim=None):
        self.df = df

        self.fig = plt.figure(figsize=(10, 9))
        self.reverse = [] if reverse is None else reverse

        for cat in self.reverse:
            if cat not in list(df):
                print(f"{cat} is not a DataFrame column")
                raise AttributeError

        rect = [0.15, 0.15, 0.65, 0.7]

        categories = sorted(list(df))

        # TODO: Assert lim var is OK
        self.max = df.max() if lim is None else lim[0]
        self.min = df.min() if lim is None else lim[1]

        labels = []
        for col in categories:
            if col not in self.reverse:
                labels.append([round(self.min[col] + i / 5.0 * (self.max[col] - self.min[col]), 2) for i in range(1, 6)])
            else:
                labels.append([round(self.max[col] - i / 5.0 * (self.max[col] - self.min[col]), 2) for i in range(1, 6)])

        self.n = len(categories)
        self.angles = np.arange(0, 360, 360.0/self.n)

        self.axes = [self.fig.add_axes(rect, projection='polar', label='axes%d' % i) for i in range(self.n)]

        self.ax = self.axes[0]
        self.ax.set_thetagrids(self.angles, labels=categories, fontsize=14, y=-0.1)

        for ax in self.axes[1:]:
            ax.patch.set_visible(False)
            ax.grid(False)
            ax.xaxis.set_visible(False)

        for ax, angle, label in zip(self.axes, self.angles, labels):
            ax.set_rgrids(range(1, 6), angle=angle, labels=label, color="grey")
            ax.spines['polar'].set_visible(False)
            ax.set_ylim(0, 5)


    def plot(self, name, *args, **kargs):
        values = []
        for col, val in sorted(zip(list(self.df), self.df.loc[name].values)):
            value = (val - self.min[col]) / (self.max[col] - self.min[col]) * 5
            if col not in self.reverse:
                values.append(value)
            else:
                values.append(5 - value)

        values = np.r_[values, values[0]]
        angle = np.deg2rad(np.r_[self.angles, self.angles[0]])

        self.ax.plot(angle, values, label=name, *args, **kargs)
        self.ax.fill(angle, values, alpha=0.1, *args, **kargs)

    def save(self, filename):
        self.ax.legend(loc=(-0.12, -0.12))
        plt.savefig(filename)
