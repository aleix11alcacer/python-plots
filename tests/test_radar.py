import pandas as pd
from pyplot.general import RadarPlot

def test_radar():
    # Load players stats from 2018-19 NBA seasson
    data = pd.read_csv("data/basketball.csv", sep=",", index_col=0)

    # Filter data
    data = data.dropna(axis=1, how='all')
    data = data.dropna(axis=0, how='any')
    data = data.loc[data['MP'] > 1000]

    # Select the desired categories
    titles = ['USG%', 'PER', 'AST%', 'BLK%', 'TOV%']

    # Create a Radar plot
    radar = RadarPlot(data[titles], reverse=['TOV%'])

    # Plot data into Radar
    radar.plot("James Harden", c="red")
    radar.plot("Stephen Curry", c="gold")

    # Save data
    radar.save("results/radarplot.png")
