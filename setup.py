import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pyplot",
    version="0.0.1",
    author="Aleix Alcacer Sales",
    author_email="aleixalcacer@gmail.com",
    description="Collection of complex pyplot",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/aleix11alcacer/",
    install_requires=[
        'pandas>=0.24.2',
        'numpy>=1.16.4',
        'matplotlib>=3.1.0'
    ],
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
